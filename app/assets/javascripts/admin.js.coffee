$ ->
  $(".admin-lightbox-event-click").bind "click", (e) ->
    e.preventDefault()
    $.ajax
      url: $(this).attr("href")
      type: "get"
      dataType: "html"
      success: (data) ->
        $.colorbox
          html: data
          innerWidth: 600
          innerHeight: 400
          fixed: true
          onComplete: ->
            latlng = new google.maps.LatLng(-22.904483, -43.210087)
            options =
              zoom: 14
              center: latlng
              streetViewControl: false
              scaleControl: false
              mapTypeControl: false
              scrollwheel: false
              zoomControl: false
              draggable: false
              scrollwheel: false
              mapTypeId: google.maps.MapTypeId.ROADMAP
      
            map = new google.maps.Map($("#google_map")[0], options)
            icon = new google.maps.MarkerImage("/assets/pin@2x.png", null, null, null, new google.maps.Size(16, 28));
            marker = new google.maps.Marker
              map: map,
              position: latlng,
              draggable: false
              clickable: false
              icon: icon
    
            latitude = $("#event_latitude").val()
            longitude = $("#event_longitude").val()
            if !!latitude && !!longitude
              marker.setPosition(new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)))
              map.setCenter(new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)))