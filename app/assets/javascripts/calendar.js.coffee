class Calendar
  
  constructor: ->
    _this = this
    $(".my-calendar").fullCalendar
      eventAfterAllRender: (view) ->
        _this.load_events()
  
  load_events: ->
    $.ajax
      url: "/calendar.json"
      type: "get"
      dataType: "json"
      success: (data) ->
        for event in data
          $(".my-calendar .fc-day[data-date=#{event.day}]").addClass("has-event")


$ ->
  if $(".my-calendar").length
    calendar = new Calendar    