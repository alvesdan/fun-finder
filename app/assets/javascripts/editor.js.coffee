class Editor
  
  init: (element) ->
    _this = this
    @element = element
    @selected_text = ""
    @start = null
    @end = null
    setInterval ->
      _this.selected_text = _this.get_text(element)
    , 100
  
  get_text: (element) ->
    if element.tagName == "TEXTAREA"
      @start = element.selectionStart
      @end = element.selectionEnd
      return element.value.substring(@start, @end)
    else
      return null
  
  replace_between: (start, end, source, replace) ->
    return source.substring(0, start) + replace + source.substring(end);
  
  bold: ->
    @element.value = @replace_between(@start, @end, @element.value , "*" + @selected_text + "*")
    true
  
  italic: ->
    full_text = 
    @element.value = @replace_between(@start, @end, @element.value , "_" + @selected_text + "_")
    true
      

$ ->
  window.editor = new Editor