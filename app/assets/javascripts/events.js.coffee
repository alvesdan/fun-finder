$ ->
  event_form = new EventForm
  $(".pagination a").addClass("btn btn-default btn-xs")

class EventForm
  
  constructor: ->
    _this = this
    @slide_position = 0
    @slide_count = 0
    $(".lightbox-event-click").bind "click", (e) ->
      e.preventDefault()
      $.ajax
        url: $(this).attr("href")
        type: "get"
        dataType: "html"
        success: (data) ->
          $.colorbox
            html: data
            innerWidth: 600
            innerHeight: 400
            fixed: true
            onComplete: ->
              _this.prepare_form()
              _this.slide_count = $(".slides-inner .slide").length
              _this.slide_position = 0
              $(".slides-inner").css("width", "#{600*_this.slide_count}px")
              $(".click-arrow.right").click ->
                _this.move_slide("right")
                return false
              $(".click-arrow.left").click ->
                _this.move_slide("left")
                return false
  
  prepare_form: ->
    @create_map()
    window.editor.init($("#event_description")[0])
    $("#event_day_string").datepicker
      format: "dd/mm/yyyy"
  
  move_slide: (direction) ->
    if direction == "right"
      return false if @slide_position == @slide_count - 1
      @slide_position++
      $(".slides-inner").css("margin-left", "-#{@slide_position * 600}px")
      @slide_moved()
    else
      return false if @slide_position == 0
      @slide_position--
      $(".slides-inner").css("margin-left", "-#{@slide_position * 600}px")
      @slide_moved()
  
  slide_moved: ->
    if @slide_position < @slide_count-1
      $(".click-arrow.left").addClass("disabled")
      $(".click-arrow.right").removeClass("disabled")
    if @slide_position > 0
      $(".click-arrow.left").removeClass("disabled")
    if @slide_position == @slide_count-1
      $(".submit-slides").show()
      $(".click-arrow.right").addClass("disabled")
    else
      $(".submit-slides").hide()
  
  create_calendar: ->
    $(".full-calendar").fullCalendar
      dayClick: (date, allDay, jsEvent, view) ->
        day = date.getDate()
        day = "0#{day}" if date.getDate() < 10
        month = date.getMonth() + 1
        year = date.getFullYear()
        $("#event_day").val("#{day}/#{month}/#{year}")
        $(".fc-day").removeClass("choosed")
        $(".fc-day[data-date='#{year}-#{month}-#{day}']").addClass("choosed")
  
  create_map: ->
    latlng = new google.maps.LatLng(-22.904483, -43.210087)
    options =
      zoom: 15
      center: latlng
      streetViewControl: false
      scaleControl: false
      mapTypeControl: false
      scrollwheel: false
      zoomControl: false
      draggable: false
      scrollwheel: false
      mapTypeId: google.maps.MapTypeId.ROADMAP
      
    map = new google.maps.Map($("#google_map")[0], options)
    autocomplete = new google.maps.places.Autocomplete $("#event_address").get(0), { types: ["geocode"], componentRestrictions: {country: "br"} }
    icon = new google.maps.MarkerImage("/assets/pin@2x.png", null, null, null, new google.maps.Size(16, 28));
    marker = new google.maps.Marker
      map: map,
      position: latlng,
      draggable: false
      clickable: false
      icon: icon
    
    latitude = $("#event_latitude").val()
    longitude = $("#event_longitude").val()
    if !!latitude && !!longitude
      marker.setPosition(new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)))
      map.setCenter(new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)))
  
    google.maps.event.addListener autocomplete, "place_changed", ->
      place = autocomplete.getPlace()
      if place.address_components.length == 7
        $("#event_zip_code").val(place.address_components[6].long_name)
      marker.setPosition(place.geometry.location)
      map.setCenter(place.geometry.location)
      $("#event_latitude").val(place.geometry.location.lat())
      $("#event_longitude").val(place.geometry.location.lng())
    
    