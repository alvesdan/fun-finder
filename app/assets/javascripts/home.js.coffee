if $("#q").length
  autocomplete = new google.maps.places.Autocomplete $("#q").get(0), { types: ["geocode"], componentRestrictions: {country: "br"} }
  google.maps.event.addListener autocomplete, "place_changed", ->
    place = autocomplete.getPlace()
    $("#address").val(place.formatted_address)
    $("#latitude").val(place.geometry.location.lat())
    $("#longitude").val(place.geometry.location.lng())
    $("#homesearch").submit()
    
$ ->
  window.user.logged (response) ->
    if (response)
      $(".footer .text-muted").remove()
  
  $("a[rel*=popover]").popover
    trigger: "hover"
    html: true