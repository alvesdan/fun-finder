map = null
markers = {}
infoboxes = []

class Event
  
  constructor: ->
    _this = this
    _this.set_map()
    $(".table-events li").each (e) ->
      data = _this.parse(this)
      icon = new google.maps.MarkerImage("/assets/pin@2x.png", null, null, null, new google.maps.Size(16, 28));
      markers[data.id] = new google.maps.Marker
        id: data.id,
        map: map,
        position: new google.maps.LatLng(data.lat, data.lng),
        draggable: false
        icon: icon
      
      google.maps.event.addListener markers[data.id], "click", ->
        _this.marker_click(this)
    
    $(".table-events li a").click ->
      id = $(this).parent().data("id")
      lat = $(this).parent().data("lat")
      lng = $(this).parent().data("lng")
      _this.open(id, lat, lng)
      
  set_map: ->
    latitude = parseFloat($("#google_map_search").attr("data-latitude"))
    longitude = parseFloat($("#google_map_search").attr("data-longitude"))
    options =
      zoom: 13
      center: new google.maps.LatLng(latitude, longitude)
      zoomControl: false
      panControl: false
      scrollwheel: false
      streetViewControl: false
      scaleControl: false
      mapTypeControl: false
      overviewMapControl: false
      mapTypeId: google.maps.MapTypeId.ROADMAP
    map = new google.maps.Map($("#google_map_search")[0], options)
    
    google.maps.Map.prototype.clearInfoBoxes = ->
      for infobox in infoboxes
        infobox.setMap(null)
      infoboxes = []
    
    $(".expand-map").click ->
      $(".google-map-canvas-fullscreen").toggleClass("expanded")
      google.maps.event.trigger(map, "resize")
    
  
  reset_map: ->
    latitude = parseFloat($("#google_map_search").attr("data-latitude"))
    longitude = parseFloat($("#google_map_search").attr("data-longitude"))
    map.setZoom(13)
    map.setCenter(new google.maps.LatLng(latitude, longitude))
  
  parse: (element) ->
    {
      id: $(element).attr("data-id"),
      lat: parseFloat($(element).attr("data-lat")),
      lng: parseFloat($(element).attr("data-lng")),
      title: $(element).find("a").html()
    }
  
  update_map: (element) ->
    data = @parse(element)
    marker = markers[data.id]
    if marker
      map.setZoom(16)
      map.setCenter(marker.getPosition())
  
  marker_click: (marker) ->
    @open_infobox(marker)
  
  open_infobox: (marker) =>
    response = $.ajax
      url: "/events/#{marker.id}"
      async: false
      type: "get"
      dataType: "json"
    .responseJSON
    map.clearInfoBoxes()
    content = "
      <div class=\"event-infobox\">
        <div class=\"date\">
          <strong>#{response.month_string}</strong>
          <span>#{response.day.split("-")[2]}</span>
        </div>
        <div class=\"name\">
          <span>#{response.hour}</span>
          <p>#{@truncate(response.name, 38)}</p>
          <a href=\"##{marker.id}\" onclick=\"window.myevent.open_from_infobox(); return false;\" class=\"btn btn-default btn-xs event-infobox-click\" data-id=\"#{marker.id}\" data-lat=\"#{response.latitude}\" data-lng=\"#{response.longitude}\">Saiba mais</a>
        </div>
      </div>"
    box_options =
      content: content
      disableAutoPan: true
      pixelOffset: new google.maps.Size(-100, -150),
      maxWidth: 0
      boxStyle: { width: "200px", height: "120px" }
      closeBoxURL: "/assets/close-infobox.png"
      closeBoxMargin: "5px",
      infoBoxClearance: new google.maps.Size(1, 1)
      isHidden: false
      pane: "floatPane"
      enableEventPropagation: false
    ib = new InfoBox(box_options)
    ib.open(map, marker)
    infoboxes.push(ib)
  
  open_from_infobox: ->
    target = ".event-infobox .btn"
    id = $(target).data("id")
    lat = $(target).data("lat")
    lng = $(target).data("lng")
    @open(id, lat, lng)
  
  truncate: (string, length) ->
    if string.length <= length
      string
    else
      string.substring(0, length) + "..."
  
  open: (id, lat, lng) ->
    $.ajax
      url: "/events/#{id}.html"
      method: "get"
      dataType: "html"
      success: (data) ->
        $.colorbox
          html: data
          innerWidth: 600
          innerHeight: 400
          fixed: true
          onComplete: ->
            $(".lightbox-container a[rel=popover]").popover
              placement: "top"
              trigger: "hover"
            latlng = new google.maps.LatLng(lat, lng)
            options =
              zoom: 16
              center: latlng
              streetViewControl: false
              scaleControl: false
              mapTypeControl: false
              scrollwheel: false
              zoomControl: false
              draggable: false
              scrollwheel: false
              mapTypeId: google.maps.MapTypeId.ROADMAP
            event_map = new google.maps.Map($("##{id}_map")[0], options)
            icon = new google.maps.MarkerImage("/assets/pin@2x.png", null, null, null, new google.maps.Size(16, 28));
            marker = new google.maps.Marker
              map: event_map,
              position: latlng,
              draggable: false
              clickable: false
              icon: icon


class Slider
  constructor: ->
    @pos = 1
    @max_pos = 5
    $(".click-arrow.right").on "click", @move
    $(".click-arrow.left").on "click", @move
    
  move: (event) =>
    direction = event.currentTarget.dataset.direction
    if (direction == "right" and @pos == 5) or (direction == "left" and @pos == 1)
      return false
    $(".table-events").removeClass("pos#{@pos}")
    @pos++ if direction == "right"
    @pos-- if direction == "left"
    $(".table-events").addClass("pos#{@pos}")
    return false
      

$ ->
  if $("#google_map_search").length
    window.myevent = new Event
    slider = new Slider