class User
  
  logged: (callback) ->
    $.ajax
      url: "/user.json"
      dataType: "json"
      type: "get"
      success: (data) ->
        callback(data.logged)

window.user = new User()