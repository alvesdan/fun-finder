class Admin::AdminController < ApplicationController
  
  before_action :authenticate_admin!
  layout "admin"
  
  def authenticate_admin!
    Rails.logger.debug(current_user.inspect)
    if current_user && current_user.is_admin?
      @admin = current_user
    else
      render nothing: true, status: 401
    end
  end
  
end