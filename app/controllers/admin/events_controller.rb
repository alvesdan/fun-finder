class Admin::EventsController < Admin::AdminController
    
  before_action :load_event, only: [:edit, :update]
    
  def index
    @events = Event.scoped
    @events = @events.search(params[:search]) if params[:search].present?
    @events = @events.for_future unless params[:past].present?
    @events = @events.benefits_to_approve if params[:pendent].present?
    @events = @events.paginate(page: params[:page], per_page: 20)
  end
  
  def edit
    render layout: false
  end
  
  def update
  end
  
  protected
  
  def load_event
    @event = Event.find(params[:id])
  end
  
end