class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  
  include Cryptor
  
  def sign_in(user)
    cookies[:user_id] = encrypt_string(user.id)
  end
  
  def current_user
    begin
      @current_user ||= User.find(decrypt_string(cookies[:user_id])) if cookies[:user_id]
    rescue Mongoid::Errors::DocumentNotFound
    rescue Mongoid::Errors::InvalidFind
      nil
    end
  end
  helper_method :current_user
  
  def authenticate_user!
    redirect_to sign_up_path unless current_user
  end
  
  def redirect_logged_in
    redirect_to events_path if current_user
  end
  
  def not_found
    render nothing: true, status: 404
  end
  
end
