class CalendarController < ApplicationController
  
  before_action :authenticate_user!
  
  def index
    @events = current_user.events.asc(:day, :hour)
    respond_to do |format|
      format.json {
        render json: @events
      }
    end
  end
  
end