module Cryptor
  
  def encryptor
    secret = "aff$cff6a-ee3b1e7-cfe@1438c29bd60c71"
    ActiveSupport::MessageEncryptor.new(secret)
  end
  
  def encrypt_string(string)
    encryptor.encrypt_and_sign(string)
  end
  
  def decrypt_string(string)
    encryptor.decrypt_and_verify(string) rescue nil
  end
  
end