class EventsController < ApplicationController
  
  before_action :authenticate_user!, only: [:index, :new, :create, :edit, :update, :destroy]
  before_action :load_event, only: [:edit, :update, :destroy]
  
  def index
    @page_title = "Meus Eventos"
    @events = current_user.events.asc(:day, :hour).paginate(page: params[:page], per_page: 5)
  end
  
  def show
    @event = Event.find(params[:id])
    respond_to do |format|
      format.html {
        render layout: false
      }
      format.json {
        render json: @event.to_json(methods: :month_string)
      }
    end
    
  end
  
  def new
    @page_title = "Novo Evento"
    @event = current_user.events.new
    render layout: false
  end
  
  def create
    @event = current_user.events.new(event_params)
    @event.save
    respond_to do |format|
      format.js
      format.html {
        @page_title = "Novo Evento"
        if @event.persisted?
          redirect_to events_path
        else
          render :new, layout: false
        end
      }
    end
    
  end
  
  def edit
    @page_title = "Editar Evento"
    render layout: false
  end
  
  def update
    @event.update(event_params)
    respond_to do |format|
      format.js
    end
  end
  
  def destroy
    @event.destroy
    redirect_to events_path
  end
  
  protected
  
  def event_params
    params.require(:event).permit(:user_id, :address, :address_complement, :name, :day_string, :hour, :url, :latitude, :longitude, :description, :zip_code, :benefit_description)
  end
  
  def load_event
    @event = current_user.events.find(params[:id])
  end
  
end
