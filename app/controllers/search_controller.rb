class SearchController < ApplicationController
  
  before_action :load_week
  
  def create
    @page_title = "Encontre a boa em #{params[:address]}"
    @search = Search.new(params[:latitude], params[:longitude])
    @events = @search.events(@beginning_of_week, @end_of_week)
  end
  
  protected
  
  def load_week
    @beginning_of_week = Date.parse(params[:start_date]) if params[:start_date].present?
    @beginning_of_week ||= DateTime.current.beginning_of_week(:sunday).to_date
    @end_of_week = @beginning_of_week + 6.days
  end
  
end
