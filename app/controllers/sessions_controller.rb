class SessionsController < ApplicationController
  
  before_action :redirect_logged_in
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.find_by(email: params[:user][:email]) rescue nil
    if @user && @user.authenticate(params[:user][:password])
      sign_in(@user)
      redirect_to events_path
    else
      @user ||= User.new
      render :new
    end
  end
  
end