class UsersController < ApplicationController
  
  before_action :redirect_logged_in, only: [:new, :create]
  
  def show
    respond_to do |format|
      format.json {
        render json: { logged: current_user ? true : false }
      }
    end
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      sign_in(@user)
      redirect_to events_path
    else
      render :new
    end
  end
  
  protected
  
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
  
end
