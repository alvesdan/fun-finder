module ApplicationHelper
  
  def error_messages_for(resource)
    render partial: "shared/error_messages", locals: { resource: resource }
  end
  
  def icon_tag(icon, content = nil)
    content_tag(:span, nil, class: "glyphicon #{icon}") + " #{content}"
  end
  
end
