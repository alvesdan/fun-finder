module EventsHelper
  
  def textile(text)
    RedCloth.new(text).to_html.html_safe
  end
  
  def url_params(start_date)
    {
      address: params[:address],
      latitude: params[:latitude],
      longitude: params[:longitude],
      start_date: start_date
    }
  end
  
end
