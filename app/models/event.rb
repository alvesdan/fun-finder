class Event
  
  attr_accessor :day_string
  
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :user_id, type: Integer
  field :name, type: String
  field :day, type: Date
  field :hour, type: String
  field :description, type: String
  field :url, type: String
  field :address, type: String
  field :address_complement, type: String
  field :zip_code, type: String
  field :latitude, type: Float
  field :longitude, type: Float
  
  field :benefit_description, type: String
  field :benefit_approved, type: Boolean, default: false
  
  field :location, type: Array
  index({location: "2d"})
  
  belongs_to :user
  
  validates :user, :name, :day, :hour, :description, :address, :zip_code, :latitude, :longitude, presence: true
  validates :name, length: { in: 15..100 }
  validates :description, length: { in: 50..400 }
  
  after_initialize :date_from_string
  before_validation :date_from_string
  after_find :string_from_date
  
  before_validation :set_location
  
  scope :search, lambda { |terms|
    any_of({:name => /.*#{terms.gsub(/\s/, ".+")}.*/i})
  }
  
  scope :nearby, lambda { |lat, lng|
    # where(
    #   latitude: {"$lte" => lat + 0.12, "$gte" => lat - 0.12},
    #   longitude: {"$lte" => lng + 0.12, "$gte" => lng - 0.12}
    # )
    # near_sphere(location: [ lat, lng ]).max_distance(location: 1)
    within_circle(
      location: [[ lat, lng ], 0.1 ]
    )
  }
  
  scope :for_week, lambda { |beginning_of_week, end_of_week|
    where(:day.gte => beginning_of_week).where(:day.lte => end_of_week)
  }
  
  scope :for_future, lambda {
    where(:day.gte => DateTime.current)
  }
  
  scope :benefits_to_approve, lambda {
    where(:benefit_description.ne => "", :benefit_description.exists => true, benefit_approved: false)
  }
  
  def full_address
    "#{address} #{' - ' + address_complement if address_complement.present?}"
  end
  
  def month_string
    I18n.l day, format: :month_string
  end
  
  def has_benefit?
    benefit_description.present?
  end
  
  def should_approve_benefit?
    has_benefit? && !benefit_approved
  end
    
  protected
  
  def date_from_string
    self.day = Time.parse("#{day_string}", "%d/%m/%Y") if day_string.present?
  end
  
  def string_from_date
    self.day_string = day.strftime("%d/%m/%Y")
  end
  
  def set_location
    self.location = [latitude, longitude]
  end
  
end
