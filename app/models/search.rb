class Search
  
  attr_accessor :address
  attr_reader :latitude, :longitude
  
  def initialize(latitude, longitude)
    @latitude = latitude.to_f
    @longitude = longitude.to_f
  end
  
  def events(beginning_of_week, end_of_week)
    Event.for_week(beginning_of_week, end_of_week).asc(:day, :hour).nearby(@latitude, @longitude).group_by(&:day)
  end
  
end