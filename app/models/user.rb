class User
  
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword
  has_secure_password validations: false
  
  field :name, type: String
  field :email, type: String
  field :password_digest, type: String
  field :is_admin, type: Boolean, default: false
  
  has_many :events
  
  validates :name, :email, presence: true
  validates :email, format: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates :email, uniqueness: true
  validates :password, presence: true, length: { minimum: 6 }, on: :create
  
  def is_admin?
    is_admin == true
  end
  
end
