FunFinder::Application.routes.draw do
  
  root to: "home#index"
  
  get "/sign-up", to: "users#new", as: :sign_up
  post "/sign-up", to: "users#create"
  get "/sign-in", to: "sessions#new", as: :sign_in
  post "/sign-in", to: "sessions#create"
  get "/search", to: "search#create", as: :search
  get "/user", to: "users#show", as: :user
  
  resources :events, only: [:index, :new, :create, :edit, :update, :destroy], path: "my-events"
  resources :events, only: [:show]
  resources :calendar, only: [:index]
  
  namespace :admin do
    resources :events, only: [:index, :show, :edit, :update]
  end
  
end
