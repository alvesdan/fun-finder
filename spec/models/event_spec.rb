require 'spec_helper'

describe Event do
  
  it "should validates user" do
    event = Event.new
    event.should have(1).error_on(:user)
  end
  
  it "should accept date from string" do
    event = Event.new(day_string: Time.current.strftime("%d/%m/%Y"))
    event.day.should eq Time.current.to_date
  end
  
end
